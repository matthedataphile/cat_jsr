$(function () {
    'use strict';
    //VARS
    //Rules: Ids are upperCase within the local environment
    var globalState = {};
    globalState.nextItemID = '';
    globalState.responseTableFel = [];
    globalState.responseObjectMat = {};
    globalState.itemListDOM = $("tr[id^='promis_']");
    globalState.questionPositionCounter = 1;
    globalState.remoteURL = 'https://cats.charite.de'; //'http://cats.charite.de:8080', 'http://localhost:8000'
    globalState.availableItemsLocalArray = [];
    globalState.itemClickHistory = [];
    globalState.latestItemClicked = '';
    globalState.catId = '';
    globalState.terminate = false;
    globalState.instrumentTitle = $('#surveytitle')[0].innerText;
    //TODO get slbuUserID in a different way
    globalState.slbuUserID = window["shared_lib_browse_url"].match(/user=(.*)(?:\b|&)/)[1]; //variable shared_lib_browse_url is defined in init_function.php, get a 32 char user id e.g.: 'user=0a1b2c3d4e5f6g7h8i9j0a1b2c3d4e5f'
    globalState.itemDisplayType = 'catClassic'; //use 'catClassic' or 'fade' (for debugging);
    c(document['jsrCatIdInj']);
    //monkey patching jquery for $.regex, polyfill
    (function ($) {
        $.fn.regex = function (pattern, fn, fn_a) {
            var fn = fn || $.fn.text;
            return this.filter(function () {
                return pattern.test(fn.apply($(this), fn_a));
            });
        };
    })(jQuery);
    showItem('noItemID'); //hides all items
    function isOnCorrectPage() {
        return $("#surveyinstructions").length;
    }
    //runs this code on the survey page only
    if (!isOnCorrectPage()) {
        c("script not executed as it is not the survey page");
        c("choose '1. Apply to:' -> 'Surveys' in the REDCap JavaScript Injector");
        return;
    }
    function informsREDCapAdmin() {
        c('please forward it to the PNC admin:');
        c('InstUserID: ' + globalState.slbuUserID);
        c('instrument Title: ' + globalState.instrumentTitle);
    }
    informsREDCapAdmin();
    function getCatId() {
        c(document['jsrCatIdInj']);
        if (document['jsrCatIdInj']) {
            globalState.catId = document['jsrCatIdInj'];
            c('CATID defined by external module');
            initCatAtRemote();
        }
        else {
            var gSheetKey = '1U4nXIY18e4dRK75TS_D9Td_TWCeEmYfny_YIXYkqBWY';
            var workSheetID = 'od6';
            $.getJSON('https://spreadsheets.google.com/feeds/list/' + gSheetKey + '/' + workSheetID + '/public/values?alt=json', function (data) {
                //catLookupData = data.feed.entry;
                var entry = data.feed.entry;
                for (var i = 0; i < entry.length; i++) {
                    var e = entry[i];
                    var tableInstUserId = e.gsx$instuserid.$t;
                    var tableInstId = e.gsx$instid.$t;
                    if (globalState.slbuUserID === tableInstUserId && globalState.instrumentTitle === tableInstId) {
                        globalState.catId = e.gsx$catid.$t;
                        c('CATID defined by remote');
                        initCatAtRemote();
                        return;
                    }
                }
                //hardCode globalState.catId
                var cIDObj = $('tr').regex(/catid.*/, $.fn.attr, ['id']);
                if (cIDObj.length) {
                    globalState.catId = cIDObj[0].outerText.trim();
                    c('globalState.catId was hard coded');
                    initCatAtRemote();
                }
                else {
                    c('WARNING: No CAT defined');
                }
            });
        }
    }
    getCatId();
    trackCatUsageWithGForm();
    function trackCatUsageWithGForm() {
        var baseURL = "https://docs.google.com/forms/d/e/1FAIpQLSfRUZgOYO1_Bvff7P_Y_FOGwM507LhlYnaec8qLOMjt-Etwkw/formResponse?'";
        var userInfo = {
            timeOpened: new Date(),
            timezone: (new Date()).getTimezoneOffset() / 60
        };
        var userData = {
            'entry.1419445876': globalState.slbuUserID,
            'entry.1458404071': JSON.stringify(userInfo) //uInfo
        };
        $.ajax({
            url: baseURL,
            data: userData
        });
    }
    //FUNCTIONS
    function availableItemsLocal() {
        $('tr').filter(function () {
            if (this.id.match(/promis_[^_-]*-tr/)) {
                globalState.availableItemsLocalArray.push(this.id.match(/promis_([^_-]*)-tr/)[1].toUpperCase());
            }
        });
        //TODO submit the available items to the server
        //c("globalState.availableItemsLocalArray:");
        //c(globalState.availableItemsLocalArray);
        return globalState.availableItemsLocalArray;
    }
    availableItemsLocal();
    function initCatAtRemote() {
        // maybe initCatAtRemote can be replaces by the post method (the first request could be as any other, just with no parameters)
        // consider sending globalState.availableItemsLocalArray
        $.ajax({
            type: 'get',
            url: globalState.remoteURL + '/' + globalState.catId + '/init_cat',
            success: function (itemDataFromCatServer) {
                if (checkIfResponseTableIsValid(itemDataFromCatServer)) {
                    //console.log('responseArray',data);
                    initVars(itemDataFromCatServer);
                    initCATatLocal();
                }
                else {
                    showItem("non-Cat");
                    c('abort');
                    errorMsg('response table not valid');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                showItem("non-Cat");
                errorMsg(thrownError);
            }
        });
    }
    function checkIfResponseTableIsValid(resp) {
        try {
            c('first item in response table: ' + resp.response_table[0].item);
        }
        catch (_a) {
            c('first entry in response_table is not defined');
            return false;
        }
        try {
            c('start item: ' + resp.start_item[0]);
        }
        catch (_b) {
            c('start_item it not defined');
            return false;
        }
        return true;
    }
    function initVars(DataFromCatServer) {
        globalState.responseTableFel = DataFromCatServer.response_table;
        globalState.responseObjectMat = responseTableFunc.FelMat(globalState.responseTableFel);
        var startItem = DataFromCatServer.start_item[0];
        if (startItem) {
            globalState.nextItemID = startItem;
        }
        else {
            showItem("non-Cat");
            c("%c WARNING: no starting Item was defined. Cat starts at first item", 'background: #222; color: #bada55');
            globalState.nextItemID = DataFromCatServer.response_table[0].item;
        }
        c(globalState.responseTableFel);
    }
    function showItem(itemID) {
        //fall-back if CAT server does not work:
        if (itemID == "non-Cat") {
            c('no CAT is shown');
            $('tr').regex(/promis_[^-_]+-tr/, $.fn.attr, ['id']).show();
            $('tr').regex(/promis_tscoref.*-tr/, $.fn.attr, ['id']).hide();
            submitOn(true);
            return;
        }
        //globalState.itemListDOM.fadeOut("slow");
        var showItemHtml = $("#promis_" + itemID.toLowerCase() + "-tr");
        submitOn(false);
        switch (globalState.itemDisplayType) {
            case 'fade':
                globalState.itemListDOM.fadeTo(1000, 0.2);
                if (itemID == 'noItemID') {
                    return;
                }
                showItemHtml.fadeTo(1000, 1);
                $('html, body').animate({
                    scrollTop: (showItemHtml.offset().top)
                }, 1000);
                break;
            case 'catClassic':
                globalState.itemListDOM.hide();
                if (itemID == 'noItemID') {
                    return;
                }
                showItemHtml.fadeTo(1000, 1);
                submitOn(false);
                break;
            default:
        }
    }
    function initCATatLocal() {
        showItem(globalState.nextItemID);
        globalState.itemListDOM.find(".choicevert input").prop("onclick", null).off("click").click(function () {
            var itemJustClicked = $(this)[0].name.match(/promis_([^_-]*)___radio/)[1].toUpperCase();
            //updates earlier
            globalState.responseObjectMat[itemJustClicked].response = this.value;
            document.forms['form']["promis_" + itemJustClicked.toLowerCase()].value = this.value;
            globalState.latestItemClicked = itemJustClicked;
            globalState.itemClickHistory.push(itemJustClicked);
            //set questionPosition
            globalState.responseObjectMat[itemJustClicked].questionPosition = globalState.questionPositionCounter;
            globalState.questionPositionCounter++;
            globalState.responseTableFel = responseTableFunc.MatFel(globalState.responseObjectMat);
            c(globalState.responseTableFel);
            sendData(JSON.stringify({ 'response_table': globalState.responseTableFel }));
        });
    }
    var responseTableFunc = {};
    var atrs = ['response', 'questionPosition', 'theta', 'theta_se'];
    responseTableFunc.FelMat = function (Arr) {
        var Obj = {};
        $.each(Arr, function (index, value) {
            var key = value.item;
            Obj[key] = {};
            $.each(atrs, function (i, atr) {
                Obj[key][atr] = (value[atr]) ? value[atr] : '';
            });
        });
        return Obj;
    };
    responseTableFunc.MatFel = function (Obj) {
        var someArray = [];
        $.each(Obj, function (itemID, itemAtr) {
            var subObj = {};
            subObj.item = itemID;
            $.each(atrs, function (i, atr) {
                if (itemAtr[atr]) {
                    subObj[atr] = itemAtr[atr];
                }
            });
            someArray.push(subObj);
        });
        return someArray;
    };
    responseTableFunc.updateResponseTable = function (responseTableMatFormat) {
        $.each(responseTableMatFormat, function (index, value) {
            //if(value.response){globalState.responseObjectMat[index].response = value.response}
            if (value.questionPosition) {
                globalState.responseObjectMat[index].questionPosition = value.questionPosition;
            }
            if (value.theta) {
                globalState.responseObjectMat[index].theta = value.theta;
            }
            if (value.theta_se) {
                globalState.responseObjectMat[index].theta_se = value.theta_se;
            }
        });
        //c(globalState.responseObjectMat);
    };
    function updateRedCap() {
        $.each(globalState.responseObjectMat, function (index, value) {
            var itID = index.toString().toLocaleLowerCase();
            if (value.theta) {
                $('#promis_' + itID + '_tscore-tr input').val(value.theta);
            }
            if (value.theta_se) {
                $('#promis_' + itID + '_stderror-tr input').val(value.theta_se);
            }
            if (value.questionPosition) {
                $('#promis_' + itID + '_qposition-tr input').val(value.questionPosition);
            }
        });
        var inputs = $('input');
        inputs.regex(/promis_tscore.*/, $.fn.attr, ['name']).val(globalState.responseObjectMat[globalState.latestItemClicked].theta);
        inputs.regex(/promis_std_error.*/, $.fn.attr, ['name']).val(globalState.responseObjectMat[globalState.latestItemClicked].theta_se);
    }
    function sendData(data) {
        $.ajax({
            type: 'post',
            url: globalState.remoteURL + '/' + globalState.catId + '/main',
            dataType: "json",
            data: data,
            success: function (Databack) {
                //TODO if item proceeds item that has a theta already it's not delivered be the server (BUG server side)?
                //TODO Databack does not deliver responsePosition
                //console.log('responseArray',data);
                //initVars(itemDataFromCatServer);
                globalState.nextItemID = Databack.next_item[0];
                globalState.terminate = Databack.terminate[0];
                responseTableFunc.updateResponseTable(responseTableFunc.FelMat(Databack["1"]));
                //TODO ask Felix to change Array key from "1" to "response_table"
                c(data);
                c(globalState.responseObjectMat);
                updateRedCap();
                nextItemOrSubmit();
            },
            error: function (error) {
                c(error);
                errorMsg(error);
            }
        });
    }
    function checkIfTerminate() {
        //TODO: max items ?
        var latestItemAtr = globalState.responseObjectMat[globalState.latestItemClicked];
        //criteria for stopping CAT
        // SE Threshold @ Local
        var SEThreshold = 0.2;
        var SET = false;
        if (latestItemAtr.theta_se) {
            if (latestItemAtr.theta_se < SEThreshold) {
                c("theta_se is lower than " + SEThreshold);
                SET = true;
            }
        }
        else {
            c('theta_se not set');
        }
        // terminate from Remote
        if (globalState.terminate || SET || globalState.nextItemID == "ENDE") {
            return true;
        }
    }
    function nextItemOrSubmit() {
        if (checkIfTerminate()) {
            submitOn(true);
            submitForm();
        }
        else {
            if (globalState.nextItemID) {
                c(console.log(globalState.nextItemID));
                showItem(globalState.nextItemID);
            }
            else {
                c("Next item not defined");
            }
        }
    }
    function submitOn(on) {
        var submitArea = $('tr[class=surveysubmit]');
        if (on) {
            submitArea.fadeTo(1000, 1.0);
            //alert("We now have sufficient data. You may submit");
            //submitForm();
        }
        else {
            submitArea.hide();
        }
    }
    function submitForm() {
        //$("form:first").trigger("submit");
        //$("button[name='submit-btn-saverecord']").click();
        $("button[name='submit-btn-saverecord']").trigger('onclick');
        //$('button:contains("Submit")').trigger('onclick');
    }
    function c(a, b) {
        if (b === void 0) { b = ''; }
        //console.log(a,b);
    }
    function errorMsg(msg) {
        alert('An error occurred:\n"' + msg + '".\nPlease, contact the admin and forward this error message');
    }
});
//# sourceMappingURL=cat_jsr.js.map